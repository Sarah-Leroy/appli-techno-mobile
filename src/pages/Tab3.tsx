import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab3.scss';
import RechercheRecettes from '../components/RechercheRecettes';

const Tab3: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar class="rougeClair">
          <IonTitle>Plus de recettes</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <ExploreContainer name="Tab 3 page" />
        <RechercheRecettes></RechercheRecettes>
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
