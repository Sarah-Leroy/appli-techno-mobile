import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { addCircleOutline, searchOutline, listOutline } from 'ionicons/icons';
import Tab1 from './pages/Tab1';
import Tab2 from './pages/Tab2';
import Tab3 from './pages/Tab3';
import {RecoilRoot} from "recoil";

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import "./App.scss"

const App: React.FC = () => (
  <IonApp>
    <RecoilRoot>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route path="/tab1" component={Tab1} exact={true} />
          <Route path="/tab2" component={Tab2} exact={true} />
          <Route path="/tab3" component={Tab3} />
          <Route path="/" render={() => <Redirect to="/tab1" />} exact={true} />
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton class="tab-vert" tab="tab1" href="/tab1">
            <IonIcon icon={listOutline} />
            <IonLabel>Mes recettes</IonLabel>
          </IonTabButton>
          <IonTabButton class="tab-bleu" tab="tab2" href="/tab2">
            <IonIcon icon={addCircleOutline} />
            <IonLabel>Gérer les recettes</IonLabel>
          </IonTabButton>
          <IonTabButton class="tab-rouge" tab="tab3" href="/tab3">
            <IonIcon icon={searchOutline} />
            <IonLabel>Plus de recettes</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
    </RecoilRoot>
  </IonApp>
);

export default App;
