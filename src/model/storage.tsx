import { Plugins } from '@capacitor/core';
import { Ingredient } from "./Ingredient"

const { Storage } = Plugins ;

export async function setRecette(titre: string, pour: string, tempsPreparation: string, ingredients: Ingredient[], instructions: string[]) {
  const recette = {"titre": titre, "pour": pour, "tempsPreparation": tempsPreparation, "ingredients": ingredients, "instructions": instructions};
  await Storage.set({
    key: titre,
    value: JSON.stringify(recette)
    });
}

export async function getRecette(titre: string) {
  const recette = await Storage.get({ key: titre });
  return JSON.parse(recette.value!);
}

export async function getTitreRecettes() {
  const { keys } = await Storage.keys();
  return keys;
}

export async function clearRecettes() {
  await Storage.clear();
}

export async function removeRecette(titre: string) {
  await Storage.remove({key : titre});
}


