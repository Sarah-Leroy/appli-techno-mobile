import  React, {useState}  from "react";
import { IonButton, IonInput, IonTextarea, IonItem, IonList, IonCol, IonSelect, IonItemGroup, IonSelectOption, IonAlert } from "@ionic/react";
import { setRecette, getTitreRecettes } from "../model/storage";
import { useRecoilState } from "recoil";
import { listeRecettesAtom } from "../model/recoil/ListeRecettesAtom";
import { Ingredient } from "../model/Ingredient"

const AjoutRecette = () => {
    const [listeRecettes, setListeRecettes] = useRecoilState(listeRecettesAtom);
    const [titre, setTitre] = useState<string>();
    const [pour, setPour] = useState<string>();
    const [temps, setTemps] = useState<string>();
    const [nomIngredient, setNomIngredient] = useState<string>();
    const [quantiteIngredient, setQuantiteIngredient] = useState<string>();
    const [uniteIngredient, setUniteIngredient] = useState<string>();
    const [instruction, setInstruction] = useState<string>();
    const [listeInstructions, setListeInstructions] = useState<string[]>([]);
    const [listeIngredients, setListeIngredients] = useState<Ingredient[]>([]);
    const [showAlertIngredient, setShowAlertIngredient] = useState(false);
    const [showAlertInstruction, setShowAlertInstruction] = useState(false);
    const [showAlertAjout, setShowAlertAjout] = useState(false);
    return(
        <>
        <IonItemGroup id="AjoutRecette">
        <IonInput class="input" placeholder="Titre" onIonChange={(e) => setTitre(e.detail.value!)}></IonInput>
        <IonInput class="input" placeholder="Pour (nombre de personnes, parts ...)" onIonChange={(e) => setPour(e.detail.value!)}></IonInput>
        <IonInput class="input" placeholder="Temps de préparation" onIonChange={(e) => setTemps(e.detail.value!)}></IonInput>
        <IonCol>
        <IonItem color="none" lines="none">
        <IonInput class="input" clearOnEdit={true} placeholder="Nom" onIonChange={(e) => setNomIngredient(e.detail.value!)}></IonInput>
        <IonInput class="input" clearOnEdit={true} placeholder="Quantité" onIonChange={(e) => setQuantiteIngredient(e.detail.value!)}></IonInput>
        <IonSelect id="select" placeholder="Unité"  onIonChange={(e) => setUniteIngredient(e.detail.value!)}>
            <IonSelectOption>cc</IonSelectOption>
            <IonSelectOption>cs</IonSelectOption>
            <IonSelectOption>g</IonSelectOption>
            <IonSelectOption>ml</IonSelectOption>
            <IonSelectOption></IonSelectOption>
        </IonSelect>
        </IonItem>
        </IonCol>
        <IonButton class="bleu" onClick={() => {
        if (nomIngredient===undefined) {
            setShowAlertIngredient(true);}
        else {
            setListeIngredients(listeIngredients.concat({"nom": nomIngredient!, "quantite": parseInt(quantiteIngredient!), "unite": uniteIngredient}));
            setNomIngredient(undefined);}
        }}> Ajouter ingredient</IonButton>
        <IonAlert isOpen={showAlertIngredient} buttons={["OK"]} onDidDismiss={() => setShowAlertIngredient(false)} message="Veuillez saisir un ingrédient."></IonAlert>
        <IonList class="transparent">
        {listeIngredients.map((ingredient) => (
            ingredient.unite && ingredient.quantite ? (<IonItem color="none" lines="none">{ingredient.nom+" : "+ingredient.quantite+" "+ingredient.unite}</IonItem>)
            : (ingredient.quantite? (<IonItem color="none" lines="none">{ingredient.nom+" : "+ingredient.quantite}</IonItem>) : (<IonItem color="none" lines="none">{ingredient.nom}</IonItem>))))}
        </IonList>
        <IonTextarea class="input" placeholder="Instructions" clearOnEdit={true} onIonChange={(e) => setInstruction(e.detail.value!)}></IonTextarea>
        <IonButton class="bleu" onClick={() => {
        if (instruction===undefined) {
            setShowAlertInstruction(true);}
        else {
            setListeInstructions(listeInstructions.concat(instruction!));
            setInstruction(undefined)}
        }}> Ajouter instruction</IonButton>
        <IonAlert isOpen={showAlertInstruction} buttons={["OK"]} onDidDismiss={() => setShowAlertInstruction(false)} message="Veuillez saisir une instruction."></IonAlert>
        <IonList class="transparent">
        {listeInstructions.map((instruction) => (
            <IonItem lines="none" color="none">{instruction}</IonItem>))}
        </IonList>
        <IonButton class="bleu" onClick={() => {
        if (listeIngredients.length===0 || listeInstructions.length===0 || titre===undefined || pour===undefined || temps===undefined )
             {setShowAlertAjout(true)}
        else {
            setRecette(titre!, pour!, temps!, listeIngredients!, listeInstructions!);
            getTitreRecettes().then(resp => setListeRecettes(resp));
            setListeInstructions([]);setListeIngredients([]);}
        }}>Ajouter la recette</IonButton>
        <IonAlert isOpen={showAlertAjout} buttons={["OK"]} onDidDismiss={() => setShowAlertAjout(false)} message="Il manque des informations (titre, nombre de parts, temps de préparation, ingrédients, instructions)."></IonAlert>
        </IonItemGroup>
        </>)
    };

export default AjoutRecette;
