import React, { useState } from "react";
import { IonList, IonItem, IonLabel } from "@ionic/react";
import DetailsRecette from "./DetailsRecette";

function masquerDiv(id) {
  if (document.getElementById(id)!.style.display === 'none') {
       document.getElementById(id)!.style.display = 'block'}
  else {document.getElementById(id)!.style.display = 'none'}};

const AffichageRecherche = ({data}) => {
  const [recetteAffichee, setRecetteAffichee] = useState(undefined);
  if (data) {
    if (data.length!==0) {
      return (
        <IonList class="transparent">
        {data.map((recette) => (
        <>
        <IonItem color="none" lines="none" button onClick={() => 
          {fetch(`https://api.spoonacular.com/recipes/${recette.id}/information?apiKey=0bbae341142c437cb965ef863d62700d`)
          .then((resp) => resp.json())
          .then((json) => setRecetteAffichee(json));masquerDiv(recette.id)}}>
        <IonLabel><strong>{recette.title}</strong></IonLabel>
        </IonItem>
        <IonItem class="beige" id={recette.id} style={{display: "none"}}>
        <DetailsRecette  titre={recette.title} recette={recetteAffichee}></DetailsRecette>
        </IonItem>
        </>
        ))}
        </IonList>)}
    else {
      return(<IonItem color="none" lines="none"><strong>Aucune recette ne correspond à votre recherche.</strong></IonItem>)}}
  else {
    return(<IonItem color="none" lines="none"><strong>Saissisez un critère puis cliquer sur RECHERCHER.</strong></IonItem>)}
};

export default AffichageRecherche;