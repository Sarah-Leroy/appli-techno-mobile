export interface Ingredient {
  nom: string;
  quantite?: number;
  unite?: string;
}