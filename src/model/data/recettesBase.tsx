export const Cookies = {
    "titre": "Cookies",
    "pour" : "25 cookies environ",
    "tempsPreparation" : "30 min",
    "ingredients": [
      { "nom": "farine", "quantite": 200, "unite": "g" },
      { "nom": "sachet de levure", "quantite": 1 },
      { "nom": "sucre", "quantite": 100, "unite": "g" },
      { "nom": "cassonade", "quantite": 50, "unite": "g" },
      { "nom": "sachet de sucre vanillé", "quantite": 1},
      { "nom": "beurre", "quantite": 120, "unite": "g" },
      { "nom": "oeuf", "quantite": 1},
      { "nom": "pépites de chocolat", "quantite": 150, "unite": "g" }
    ],
    "instructions": ["Préchauffer le four à 180° C.", "Mélanger le beurre et la cassonade dans un saladier.", "Bien fouetter pour obtenir une texture crémeuse, puis ajouter l’œuf.", "Incorporer la farine, la levure et le chocolat. Mélanger le tout rapidement sans trop travailler la pâte.", "Sur une plaque de cuisson recouverte de papier sulfurisé, former et déposer des petites boules, les aplatir légèrement. Enfourner 10 minutes environ.", "Sortir la plaque du four, laisser refroidir avant de décoller les cookies."]
  };

export const Sables = {
    "titre": "Sablés",
    "pour" : "30 sablés environ",
    "tempsPreparation" : "40 min",
    "ingredients": [
      { "nom": "farine", "quantite": 250, "unite": "g" },
      { "nom": "sucre", "quantite": 125, "unite": "g" },
      { "nom": "sachet de sucre vanillé", "quantite": 1 },
      { "nom": "beurre", "quantite": 125, "unite": "g" },
      { "nom": "oeuf", "quantite": 1}
    ],
    "instructions": ["Dans un saladier, mélangez la farine, le sucre, le sucre vanillé et le sel. Ajoutez le beurre froid coupé en petits morceaux et travaillez l’ensemble du bout des doigts jusqu’à obtention d’une poudre fine.", "Incorporez l’œuf et mélangez pour obtenir une pâte homogène. Rassemblez-la en boule emballez-la dans du film alimentaire et réservez-la pendant 1 heure au réfrigérateur.", "Abaissez la pâte, puis découpez-la à l’aide d’emporte-pièces de différentes formes. Déposez les biscuits sur la plaque de cuisson recouverte de papier sulfurisé (ou une toile en silicone).", "Faites cuire 12 à 15 minutes. Quand les bords commencent à dorer, c’est que c’est bon."]
  };

  export const GalettesBrocoli = {
    "titre": "Galettes brocoli",
    "pour" : "8 galettes environ",
    "tempsPreparation" : "1h",
    "ingredients": [
      { "nom": "brocoli", "quantite": 1 },
      { "nom": "farine", "quantite": 50, "unite": "g" },
      { "nom": "parmesan râpé", "quantite": 60, "unite": "g" },
      { "nom": "oeuf", "quantite": 1},
      { "nom": "gousse d'ail", "quantite": 1 },
      { "nom": "huile d'olive", "quantite": 1, "unite": "cs" },
      { "nom": "chapelure", "quantite": 2, "unite": "cs" },
      { "nom": "sel"},
      { "nom": "poivre"}
    ],
    "instructions": ["Cuire le brocoli 7-8 minutes dans une casserole d'eau bouillante puis le passer sous l'eau froide et bien égoutter.",
    "Dans un saladier battre l’œuf en omelette et ajouter l’oignon ainsi que la farine, la chapelure, le parmesan, du sel et du poivre.",
    "Écraser le brocoli à la fourchette puis l’incorporer à la précédente préparation, bien mélanger pour homogénéiser le tout.",
    "Faire chauffer l'huile d'olive dans une poêle antiadhésive puis cuire les galettes en prélevant une bonne cuillère à soupe de la préparation à aplatir avec le dos de la cuillère pour donner une forme ronde.",
    "Cuire 2-3 minutes de chaque côté, les galettes doivent être légèrement dorées."]
  };

  export const CrumbleButternut = {
    "titre": "Crumble butternut",
    "pour" : "6 parts",
    "tempsPreparation" : "1h15",
    "ingredients": [
      { "nom": "butternut", "quantite": 1 },
      { "nom": "farine", "quantite": 150, "unite": "g" },
      { "nom": "parmesan râpé", "quantite": 100, "unite": "g" },
      { "nom": "beurre", "quantite": 100, "unite": "g"},
      { "nom": "gousse d'ail", "quantite": 1 },
      { "nom": "oignon", "quantite": 1},
      { "nom": "sel"},
      { "nom": "poivre"}
    ],
    "instructions": ["Eplucher le butternut, le couper en dés et le faire cuire le butternut à l’eau (environ 15 min).",
      "Faire revenir l’oignon et l’ail dans une poêle puis mélanger au butternut.",
      "Mélanger la farine, le beurre et le parmesan avec les mains pour faire le crumble.",
      "Verser le butternut dans le plat et ajouter le crumble au-dessus.",
      "Cuire environ 30 minutes à 200°."
      ]
  };