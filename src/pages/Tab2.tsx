import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab2.scss';
import AjoutRecette from "../components/AjoutRecette";
import SuppressionRecette from "../components/SuppressionRecette";

const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar class="bleuClair">
          <IonTitle>Ajouter une recette</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Ajouter une recette</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Tab 2 page" />
        <AjoutRecette></AjoutRecette>
        <IonToolbar class="bleuClair">
          <IonTitle>Supprimer</IonTitle>
        </IonToolbar>
        <SuppressionRecette></SuppressionRecette>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
