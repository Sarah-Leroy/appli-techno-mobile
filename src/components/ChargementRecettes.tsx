import { IonItem, IonButton } from "@ionic/react";
import React from "react";
import { setRecette, getTitreRecettes } from "../model/storage";
import { Cookies, Sables, GalettesBrocoli, CrumbleButternut } from "../model/data/recettesBase";
import { useRecoilState } from "recoil";
import { listeRecettesAtom } from "../model/recoil/ListeRecettesAtom";

const ChargementRecettes = () => {
    const [listeRecettes, setListeRecettes] = useRecoilState(listeRecettesAtom);
    return(
        <>
        <IonItem class="beige">
        <IonButton class="vert" onClick={() => {setRecette(Cookies.titre, Cookies.pour, Cookies.tempsPreparation, Cookies.ingredients, Cookies.instructions );
            setRecette(Sables.titre , Sables.pour, Sables.tempsPreparation, Sables.ingredients, Sables.instructions);
            setRecette(GalettesBrocoli.titre, GalettesBrocoli.pour, GalettesBrocoli.tempsPreparation, GalettesBrocoli.ingredients, GalettesBrocoli.instructions);
            setRecette(CrumbleButternut.titre, CrumbleButternut.pour, CrumbleButternut.tempsPreparation, CrumbleButternut.ingredients, CrumbleButternut.instructions);
            getTitreRecettes().then(resp => setListeRecettes(resp))
            }}>Charger quelques recettes sympas</IonButton>
        </IonItem>
        </>
    )
};

export default ChargementRecettes;

