import  React, {useState}  from "react";
import { IonButton, IonInput, IonItemGroup, IonItem, IonSelect, IonLabel, IonSelectOption, IonAlert } from "@ionic/react";
import AffichageRecherche from "./AffichageRecherche";

const RechercheRecettes = () => {
    const [data, setData] = useState(undefined);
    const [query, setQuery] = useState<string>();
    const [ingredient1, setIngredient1] = useState<string>();
    const [ingredient2, setIngredient2] = useState<string>();
    const [nombreRecettes, setNombreRecettes] = useState(10);
    const [showAlertRecherche, setShowAlertRecherche] = useState(false);
    return(
        <>
        <IonItem>Rechercher une recette (critères en anglais) : </IonItem>
        <IonItem lines="none">
        <IonInput  clearOnEdit={true} placeholder="Mot clé" onIonChange={(e) => setQuery(e.detail.value!)}>Par mot clé : </IonInput>
        </IonItem>
        <IonItem lines="none">OU Par ingrédient(s) : </IonItem>
        <IonInput clearOnEdit={true} placeholder="Ingrédient" onIonChange={(e) => setIngredient1(e.detail.value!)}></IonInput>
        <IonInput clearOnEdit={true} placeholder="Ingrédient" onIonChange={(e) => setIngredient2(e.detail.value!)}></IonInput>
        <IonItem>
        <IonLabel>Nombre de recettes à afficher</IonLabel>
        <IonSelect placeholder="Par défaut : 10" onIonChange={(e) => setNombreRecettes(e.detail.value!)}>
            <IonSelectOption>10</IonSelectOption>
            <IonSelectOption>20</IonSelectOption>
            <IonSelectOption>30</IonSelectOption>
            <IonSelectOption>40</IonSelectOption>
            <IonSelectOption>50</IonSelectOption>
        </IonSelect>
        </IonItem>
        <IonItem class="beige">
        <IonButton class="rouge" onClick={() => {
        if (query===undefined && ingredient1===undefined && ingredient2===undefined) {
            setShowAlertRecherche(true)}
        else {
            if (query!==undefined) {
                fetch(`https://api.spoonacular.com/recipes/search?apiKey=0bbae341142c437cb965ef863d62700d&query=${query}&number=${nombreRecettes}&instructionsRequired=true`)
                .then((resp) => resp.json())
                .then((json) => setData(json.results));
                setQuery(undefined)}
            if (ingredient1!==undefined || ingredient2!==undefined) {
                fetch(`https://api.spoonacular.com/recipes/findByIngredients?apiKey=0bbae341142c437cb965ef863d62700d&ingredients=${ingredient1},+${ingredient2}&number=${nombreRecettes}&instructionsRequired=true`)
                .then((resp) => resp.json())
                .then((json) => setData(json));
                setIngredient1(undefined);setIngredient2(undefined)}
        }}}>Rechercher</IonButton>
        <IonAlert isOpen={showAlertRecherche} buttons={["OK"]} onDidDismiss={() => setShowAlertRecherche(false)} message="Veuillez saisir un critère de recherche (mot clé ou ingrédient(s))."></IonAlert>
        </IonItem>
        <IonItemGroup id="AffichageRecette"> 
        <IonItem class="beige">Cliquez sur le titre de la recette pour voir les détails</IonItem>
        <AffichageRecherche data={data}></AffichageRecherche>
        </IonItemGroup>
        </>)};

export default RechercheRecettes;