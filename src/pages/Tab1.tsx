import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.scss';
import Recette from "../components/Recette";
import ChoixRecette from '../components/ChoixRecette';
import ChargementRecettes from '../components/ChargementRecettes';

const Tab1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar class="vertClair">
          <IonTitle>Mes recettes</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <ExploreContainer name="Tab 1 page" />
        <ChargementRecettes></ChargementRecettes>
        <ChoixRecette></ChoixRecette>
        <Recette></Recette>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
