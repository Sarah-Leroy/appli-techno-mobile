import {Ingredient} from "./Ingredient";

export interface Recette {
  titre: string;
  pour: string,
  temps_preparation : string,
  ingredients: Ingredient[];
  instructions: string[];
  photo?: string;
}
