import { IonItem, IonLabel, IonSelect, IonSelectOption } from "@ionic/react";
import React, { useState } from "react";
import { useRecoilState } from "recoil";
import { getRecette, getTitreRecettes } from "../model/storage";
import { RecetteAtom } from "../model/recoil/RecetteAtom";
import { listeRecettesAtom } from "../model/recoil/ListeRecettesAtom";


const ChoixRecette = () => {
    const [listeRecettes, setListeRecettes] = useRecoilState(listeRecettesAtom);
    const [recette, setRecetteAffiche] = useRecoilState(RecetteAtom);
    const [i, seti] = useState(0);
    if (listeRecettes.length===0 && i===0) {
      getTitreRecettes().then(resp => setListeRecettes(resp));
      seti(1)
    }
    return(
      <>
      <IonItem>
      <IonLabel>Afficher une recette</IonLabel>
      <IonSelect placeholder="Recette à afficher" onIonChange={event => getRecette(event.detail.value).then(resp => setRecetteAffiche(resp!))}>
      {listeRecettes.map((titre : string) => 
        <IonSelectOption>{titre}</IonSelectOption>)}
      </IonSelect>
      </IonItem>
      </>
    )
};

export default ChoixRecette;