# Application de gestion de recettes
 
Application développée dans le cadre du cours de Technologies mobile (version web) par Sarah Leroy  
Option de 2ème année Ensai : développement web & mobile  
Formation dispensée par Olivier Levitt, disponible [ici](https://formations.levitt.fr/)

## Installation avec nodeJs et Ionic

1. Installer les dépendances :
`npm install`

2. Lancer l'application dans le naviguateur :
`ionic serve`

## Documentation

### Menu 1 : Mes recettes
* Charger quelques recettes sympas : des recettes inclues pour commencer à utiliser l'application
* Afficher une recette (choisie grâce à un menu déroulant)

### Menu 2 : Gérer les recettes
* Ajouter une recette : 
  * Saisir le titre, le nombre de parts et le temps de préparation.
  * Saisir les ingrédients un par un (nom + quantité et unité si nécessaire) et cliquer sur "AJOUTER INGREDIENT" entre chaque ajout (la liste des ingrédients ajoutés s'affiche au fur et à mesure).
  * Saisir les instructions une par une et cliquer sur "AJOUTER INSTRUCTION" entre chaque ajout (la liste des instructions ajoutées s'affiche au fur et à mesure).
  * Cliquer sur "AJOUTER LA RECETTE".
* Supprimer une recette (choisie grâce à un menu déroulant)
* Supprimer toutes les recettes

### Menu 3 : Plus de recettes (recherche sur l'API [Spoonacular](https://spoonacular.com/food-api))

* Critères de recherche saisis en ANGLAIS (pas trouvé d'API en français qui convienne) :
  * Mot clé (exemple : pie)
  * Un ou deux ingrédients (exemples : apple ou apple et chocolate).
  * Nombre de recettes à afficher (choisi grâce à un menu déroulant : 10, 20, 30, 40 ou 50)
* Affichage des résulats :
  * Cliquer sur le titre de la recette pour voir le détails.
  * Cliquer sur le bouton "AJOUTER" présent à la suite des informations de la recette pour sauvegarder la recette dans l'application.

## Spécificités 

* Utilisation de [l'API Capacitor Storage](https://capacitor.ionicframework.com/docs/apis/storage) pour stocker les recettes en local 
* Utilisation de [Recoil](https://recoiljs.org/) : state management library