import React from "react";
import { IonLabel, IonList, IonItem } from "@ionic/react";
import { RecetteAtom } from "../model/recoil/RecetteAtom";
import { useRecoilState } from "recoil";


const Recette = () => {
  const [recette, setRecetteAffiche] = useRecoilState(RecetteAtom);
  if (recette) {
    return(
      <>
      <IonItem class="recette" >
      <IonLabel class="transparent">
      <h1 >{recette.titre}</h1>
      <br></br>
      <h2><strong>Pour : </strong>{recette.pour}</h2>
      <h2><strong>Temps de préparation : </strong>{recette.tempsPreparation}</h2>
      <br></br>
      <h2><strong>Ingrédients</strong></h2>
      <IonList class="transparent">
      {recette.ingredients.map((ingredient) => (
          ingredient.unite && ingredient.quantite ? (<IonItem color="none" lines="none">- {ingredient.nom+" : "+ingredient.quantite+" "+ingredient.unite}</IonItem>)
          : (ingredient.quantite? (<IonItem color="none" lines="none">- {ingredient.nom+" : "+ingredient.quantite}</IonItem>) : (<IonItem color="none" lines="none">- {ingredient.nom}</IonItem>))))}
      </IonList>
      <h2><strong>Instructions</strong></h2>
      <IonList class="transparent">
      {recette.instructions.map((instruction, index=1) => (
        <IonItem color="none" lines="none"><IonLabel class="ion-text-wrap">{index+1}. {instruction}</IonLabel></IonItem>))}
      </IonList>
      </IonLabel>
      </IonItem>
      </>)}
  else {return (<IonItem class="recette"></IonItem>)}};

export default Recette;
