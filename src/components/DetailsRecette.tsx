import React from "react";
import { IonItem, IonList, IonLabel, IonButton, IonItemGroup } from "@ionic/react";
import { setRecette, getTitreRecettes } from "../model/storage";
import { useRecoilState } from "recoil";
import { listeRecettesAtom } from "../model/recoil/ListeRecettesAtom";

function ajouterRecette(titre, recette) {
    if (recette){
      const listeIngredients = recette.extendedIngredients.map((ingredient) => ({"nom": ingredient.name, "quantite": ingredient.amount, "unite": ingredient.unit }))
      const listeInstructions = recette.analyzedInstructions[0].steps.map((instruction) => instruction.step);
      setRecette(titre, recette.servings, JSON.stringify(recette.readyInMinutes).concat(" minutes"), listeIngredients, listeInstructions)}};

const DetailsRecette = ({ titre, recette }) => {
    const [listeRecettes, setListeRecettes] = useRecoilState(listeRecettesAtom);
    if (recette) {
      return (
        <>
        <IonItemGroup class="beige">
        <IonLabel class="transparent">
        <h2><strong>Pour : </strong>{recette.servings} parts</h2>
        <h2><strong>Temps de préparation : </strong>{recette.readyInMinutes} minutes</h2>
        <br></br>
        <h2><strong>Ingrédients</strong></h2>
        <IonList class="transparent">
        {recette.extendedIngredients.map((ingredient) => 
          <IonItem color="none" lines="none" >{ingredient.original}</IonItem>)}
        </IonList>
        <h2><strong>Instructions</strong></h2>
        <IonItem lines="none" color="none"><IonLabel class="ion-text-wrap">{recette.instructions}</IonLabel></IonItem>
        <IonButton class="rouge" onClick={() => {ajouterRecette(titre, recette); getTitreRecettes().then(resp => setListeRecettes(resp))}}>Ajouter</IonButton>
        </IonLabel>
        </IonItemGroup>
        </>)}
    else {return(<IonItem></IonItem>)}};

export default DetailsRecette;

  