import { IonItem, IonLabel, IonSelect, IonSelectOption, IonButton, IonAlert } from "@ionic/react";
import React, { useState } from "react";
import { useRecoilState } from "recoil";
import { getTitreRecettes, removeRecette, clearRecettes } from "../model/storage";
import { listeRecettesAtom } from "../model/recoil/ListeRecettesAtom";

const SuppressionRecette = () => {
    const [listeRecettes, setListeRecettes] = useRecoilState(listeRecettesAtom);
    const [showAlertSuppression, setShowAlertSuppresion] = useState(false);
    return(
        <>
        <IonItem>
        <IonLabel>Supprimer la recette</IonLabel>
        <IonSelect placeholder="Recette à supprimer" onIonChange={event => {removeRecette(event.detail.value);getTitreRecettes().then(resp => setListeRecettes(resp))}}>
        {listeRecettes.map((titre : string) => 
          <IonSelectOption>{titre}</IonSelectOption>)}
        </IonSelect>
        </IonItem>
        <IonItem class="beige">
        <IonButton id="boutonBleu" onClick={ () => setShowAlertSuppresion(true)}>Supprimer toutes les recettes</IonButton>
        <IonAlert isOpen={showAlertSuppression} 
          buttons={[{text: 'ANNULER', role: 'cancel'},{text: "OK", handler: () => {clearRecettes();getTitreRecettes().then(resp => setListeRecettes(resp))}}]}
          onDidDismiss={() => setShowAlertSuppresion(false)} 
          message="Supprimer toutes les recettes ?"></IonAlert>
        </IonItem>
        </>)};

export default SuppressionRecette;