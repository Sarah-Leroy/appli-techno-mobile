import { atom } from "recoil";

export const listeRecettesAtom = atom({
    key: 'listeRecettes',
    default: []
})